var map;
var featureGroup;
var terremoto;

function onload() {

    
    var url = 'https://api.mapbox.com/styles/v1/mapbox/streets-v11/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw';
    
    map = L.map(mapid, {
        center: [32.084465336061285, 34.775333404541016],
        zoom: 2.5
    });
    L.tileLayer(url).addTo(map);

    fetch('/countries-low.json', {
        method: 'GET',
    })
    .then(function (res) { 
        // console.log(res);
        return res.json();
    })
    .then(function (data) {
        // console.log(JSON.stringify(data).length);
        // console.log(data);
        L.geoJSON(data, {
            style: function (feature) {
                return {
                    fillOpacity: 0,
                    weight: 0.3,
                    fillColor: '#F00',
                }
            },
            onEachFeature: function(feature, layer) {
                layer.on({
                    mouseover: function() {
                        layer.setStyle({fillOpacity: 0.1});
                    },
                    mouseout: function() {
                        layer.setStyle({fillOpacity: 0});
                    }
                })
            }
        }).addTo(map);     
    })
    .catch(err => console.log(err));


    fetch('/earthquake.json', {
        method: 'GET',
    })
    .then(function (res) { 
        // console.log(res);
        return res.json();
    })
    .then(function (data) {
        // console.log(JSON.stringify(data).length);
        // console.log(data);
        terremoto = L.geoJSON(data, {
            style: function (feature) {
                return {
                    fillOpacity: 0.1,
                    fillColor: '#000',
                    color: '#F00',
                    opacity: 0.3,
                }
            },
            pointToLayer: function(geoJsonPoint, lating) {
                var html = '';
                for (const prop in geoJsonPoint.properties) {
                    if(geoJsonPoint.properties[prop] !== null) {
                        html += '<strong>' + prop + '<strong>: ' + geoJsonPoint.properties[prop] + '<br/>';
                    }
                }
                return L.circle(lating, 50000*(geoJsonPoint.properties.mag)).bindPopup(html);
            }
        }).addTo(map);
        setTimeout(() => {
        terremoto.bringToFront();
        }, 1000);
        
    })
    .catch(err=> console.log(err));




    // var shapesStyle = {
    //     radius: 500,
    //     color: 'red',
    //     fillColor: 'blue',
    //     fillOpacity: 0.5,
    // };

    // L.circle([32.084465336061285, 34.775333404541016], shapesStyle)
    //     .addTo(map).bindPopup('I\'m  a circle');

    // L.polygon([
    //     [ 32.125471012333854, 34.78443145751953],
    //     [ 32.10715160278976, 34.77619171142578],
    //     [ 32.097263409957804, 34.7882080078125],
    //     [ 32.117329506443475, 34.78975296020508],
    //     [ 32.13128592898028, 34.791812896728516],
    //     [ 32.125471012333854, 34.78443145751953]
    // ], shapesStyle).addTo(map).bindPopup('I\'m  a polygone');

    var customIcon = L.icon({
        iconUrl: 'assets/marker.svg',
        iconSize: [40, 40],
        iconAnchor: [20, 47],
        popupAnchor: [0, -55]
    });
    var customIconBlue = L.icon({
        iconUrl: 'assets/marker-blue.svg',
        iconSize: [40, 40],
        iconAnchor: [20, 47],
        popupAnchor: [0, -55]
    });

    // var customPopup = L.popup({
    //         maxWidth: 400,
    //         minWidth: 100,
    //     })
    //     .setLatLng([32.076865336061285, 34.775333404541016])
    //     .setContent('<a href="https://placeholder.com"> <img src="https://via.placeholder.com/100" alt=""></a>');
    // // .openOn(map);


    // L.marker([32.076865336061285, 34.775333404541016])
    //     .addTo(map)
    //     .bindPopup(customPopup);

    // L.marker([32.084465336061285, 34.775333404541016], {
    //         icon: customIcon
    //     })
    //     .addTo(map)
    //     .bindPopup('I\'m  a marker')
    //     .openPopup();



    // var json = {"type":"FeatureCollection","features":[{"type":"Feature","properties":{},"geometry":{"type":"Polygon","coordinates":[[[34.789581298828125,32.0879558976284],[34.788358211517334,32.08719234867814],[34.78852987289429,32.086047013290475],[34.78975296020508,32.08555615087292],[34.79093313217163,32.08579249236604],[34.79146957397461,32.08628335351415],[34.79146957397461,32.087028730215906],[34.791083335876465,32.08761048341734],[34.79121208190917,32.0904464772378],[34.789581298828125,32.0879558976284]]]}},{"type":"Feature","properties":{"pencil":true},"geometry":{"type":"Point","coordinates":[34.78668451309204,32.092427984785495]}},{"type":"Feature","properties":{},"geometry":{"type":"LineString","coordinates":[[34.79153394699096,32.09111910388372],[34.792606830596924,32.08962841113035],[34.79348659515381,32.08875579920955],[34.79546070098877,32.08788317895563],[34.7957181930542,32.09030104379843],[34.79477405548096,32.092209839270645],[34.794301986694336,32.09391863186338],[34.791340827941895,32.09379138245256],[34.78904485702515,32.09388227490694],[34.78627681732177,32.09404588109695],[34.784131050109856,32.094064059544415],[34.78329420089722,32.09393681033615],[34.78282213211059,32.09090095524381],[34.78516101837158,32.09088277616696],[34.78498935699463,32.08964659045676],[34.78612661361694,32.08986474209148],[34.78591203689575,32.08901031188061]]}},{"type":"Feature","properties":{},"geometry":{"type":"Point","coordinates":[34.789602756500244,32.09262795104986]}},{"type":"Feature","properties":{},"geometry":{"type":"Point","coordinates":[34.78803634643555,32.09101002962886]}},{"type":"Feature","properties":{"pencil":true},"geometry":{"type":"Point","coordinates":[34.78621244430542,32.087501404974184]}},{"type":"Feature","properties":{},"geometry":{"type":"Polygon","coordinates":[[[34.78395938873291,32.07953832696122],[34.793851375579834,32.07953832696122],[34.793851375579834,32.08395628469357],[34.78395938873291,32.08395628469357],[34.78395938873291,32.07953832696122]]]}}]};

    // L.geoJSON(json, {
    //     style: function (feature) {
    //         // can add manually properties on feature.properties variable
    //         // console.log(feature);
    //         return {
    //             color: '#000',
    //             weight: feature.geometry.type === 'Polygon' ? 2 : 10,
    //         };
    //     },
    //     pointToLayer: function (geoJsonPoint, lating) {
    //         console.log(geoJsonPoint);
    //         console.log(lating);
    //         return L.marker(lating,
    //             geoJsonPoint.properties.pencil ? {
    //                 icon: customIcon
    //             } : null
    //         );
    //     },
    //     onEachFeature: function (feature, layer) {
    //         if(feature.geometry.type === 'Point') {
    //             layer.bindPopup(feature.geometry.coordinates[0] + '');
    //         }
    //     }
    // }).addTo(map);



    // More Events on: https://leafletjs.com/reference-1.6.0.html#map-event

    map.on({
        'mousemove': function (event) {
            // console.log(event);
            var latLng = marker2.getLatLng();
            var from = turf.point([event.latlng.lat, event.latlng.lng]);
            var to = turf.point([latLng.lat, latLng.lng]);
            var distance = turf.distance(from, to, {
                units: 'kilometers'
            });
            marker2.setIcon(distance < 0.6 ? customIcon : customIconBlue);
        },
        'moveend': function (event) {
            console.log(event);
            var center = map.getCenter();
            latlng.innerText = center.lat + ' ---- ' + center.lng;
        }
    });


    var marker1 = L.marker([32.092427984785495, 34.78668451309204], {
        icon: customIconBlue
    }).on({
        'mouseover': function (event) {
            event.target.setIcon(customIcon);
        },
        'mouseout': function (event) {
            event.target.setIcon(customIconBlue);
        }
    });


    var marker2 = L.marker([32.09262795104986, 34.789602756500244], {
        icon: customIconBlue
    });
    var marker3 = L.marker([32.087501404974184, 34.78621244430542]);

    featureGroup = L.featureGroup([marker1, marker2, marker3]).addTo(map);
    // console.log(featureGroup);

    // map.fitBounds(featureGroup.getBounds(), {
    //     padding: [300, 300]
    // });




}


function search() {

    console.log(myInput.value);
    
    terremoto.eachLayer(function (layer) {
        if (layer.feature.properties.place.toLowerCase().indexOf(myInput.value.toLowerCase()) != -1) {
            layer.addTo(map);
        } else {
            map.removeLayer(layer);
        }
    })
    

}

function toggleMarkers() {
    if (map.hasLayer(featureGroup)) {
        map.removeLayer(featureGroup);
    } else {
        featureGroup.addTo(map);
    }
}
