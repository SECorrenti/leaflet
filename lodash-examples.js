var items = [
    {
        id: 5,
        name: "Efrat",
        age: 45,
        likes: 50,
        isActive: false,
    },
    {
        id: 1,
        name: "Efi",
        age: 25,
        likes: 50,
        isActive: true,
    },
    {
        id: 3,
        name: "Yaron",
        age: 21,
        likes: 20,
        isActive: true,
    },
    {
        id: 2,
        name: "Sharon",
        age: 21,
        likes: 35,
        isActive: true,
    },
];

var itemsRes = _.each(items, function (item, index) {
    // console.log(item);
    // console.log(index);
});

var mapRes = _.map(items, 'age');

// console.log(itemsRes);
// console.log(mapRes);

// var withoutRes = _.without(items, items[1]);

// var removeRes = _.remove(items, function (item) {
//     return item.id === 2;
// });

// console.log(withoutRes);

// var sortRes = items.sort(function (a, b) {
//     return b.likes - a.likes;
// });

// var sortResLodash = _.orderBy(items, ['likes', 'name'], ['desc', 'asc']);

// console.log(items);
// console.log(sortResLodash);

console.log(_.chain(items).filter('isActive').orderBy('age').first().value());
console.log(_.head([1,2,3]));
console.log(_.first([1,2,3]));
console.log(_.tail([1,2,3]));
console.log(_.last([1,2,3]));
console.log(_.initial([1,2,3]));

console.log(_.flattenDeep([1,2,3,[4,5,6,[7,8,9], 10]]))











